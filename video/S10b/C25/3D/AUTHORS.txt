################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/common/AUHTORS.txt for all contributors.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Revision history is available at 
#     https://gitlab.com/kosys/kosys-ep03-part4 . 
#
# 注記: 
#   - 更新履歴は以下からご覧いただけます。 
#     https://gitlab.com/kosys/kosys-ep03-part4  
#

井二かける
### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ###
小澤佑太	背景用モデリング,テクスチャ,著作権譲受済
井二かける	部屋設定
sacorama	下書用モデリング
廣田智亮	屋外テクスチャ,美術設定,著作権譲受済
