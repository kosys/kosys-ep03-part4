################################################################################
# This file is contributors list of this directory and its sub directories. 
# See <repository root>/common/AUHTORS.txt for all contributors.
#-------------------------------------------------------------------------------
# このファイルはこのディレクトリおよびサブディレクトリの貢献者リストです。
# すべての貢献者は、<リポジトリルート>/AUTHORS.txtをご覧ください。 
################################################################################
#
# Note: 
#   - Revision history is available at 
#     https://gitlab.com/kosys/kosys-ep03-part4 . 
#
# 注記: 
#   - 更新履歴は以下からご覧いただけます。 
#     https://gitlab.com/kosys/kosys-ep03-part4  
#

Melodic Taste
T-182
swell
えつむ
がんくま
べ～
ますっち
上原一之龍（合同会社エフドア）
中嶋有志
井二かける
冷水優果
夏野未来
小豆戒斗
岡松　仗
村尾祥平
東條あいり
桜瀬尋
武川楓
真希那稜
矢塚翔
空りんご
芋
那々海ゆあ
鈴郷（さとー）
雲丹
香月
髙咲　舞
### END AUTO-GENERATED LIST.  LEAVE THIS LINE HERE; SCRIPTS LOOK FOR IT. ###
がんくま	音響効果,サウンドミキサー,効果音提供
井二かける	効果音提供
夏野未来	効果音提供
