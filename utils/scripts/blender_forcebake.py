import bpy
import hashlib

# Bake smoke simulations
# Reference: https://blender.stackexchange.com/questions/4963/baking-smoke-on-headless-machine

targets = [
    (s, vl, o, m) 
    for s in bpy.data.scenes 
    for vl in s.view_layers 
    for o in vl.objects 
    for m in o.modifiers 
    if m.type == 'FLUID' and m.fluid_type == 'DOMAIN' and m.domain_settings.domain_type == 'GAS'
]

for s, vl, o, m in targets:
    print(f"Starting to bake all objects in the view layer '{vl.name}' of the scene '{s.name}'...")
    with bpy.context.temp_override(scene=s, view_layer=vl, active_object=o, object=o):
        print(f"Baking '{o.name}'...")
        # Set the cache directory to the .gitignore'd unique directory
        hash=hashlib.md5(f"{s.name}/{vl.name}/{o.name}".encode()).hexdigest()
        m.domain_settings.cache_directory = f"//.bake/{hash}"
        # Ensure we bake across all frames using 'ALL'
        m.domain_settings.cache_type = 'ALL'
        bpy.ops.fluid.bake_all()
